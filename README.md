Bazgu Session Node
==================

This program **session-node** is one of the nodes in a Bazgu infrastructure.
A session-node is responsible for delivering messages to online users.
The node also routes sending messages to a corresponding account-node.

Workflow
--------

An account-node opens a session in a session-node and obtains a token.
A user then connects to the session-node with the token and waits for
messages using long polling technique. Thus the user
maintains a persistent connection to a session-node.
Inactive sessions are expired in ten seconds.

Configuration
-------------

See `config.js` for details.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
