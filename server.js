process.chdir(__dirname)

require('./lib/LoadConfig.js')(loadedConfig => {

    var version = process.argv[2]

    var url = require('url')

    var Error404Page = require('./lib/Error404Page.js'),
        Log = require('./lib/Log.js')

    var chooseAccountNode = require('./lib/ChooseAccountNode.js')(loadedConfig)

    var sessions = Object.create(null)
    var sendingFiles = Object.create(null)

    var pages = Object.create(null)
    pages['/'] = require('./lib/Page/Index.js')
    pages['/accountNode/checkFile'] = require('./lib/Page/AccountNode/CheckFile.js')(sendingFiles)
    pages['/accountNode/checkSession'] = require('./lib/Page/AccountNode/CheckSession.js')(sessions)
    pages['/accountNode/close'] = require('./lib/Page/AccountNode/Close.js')(sessions)
    pages['/accountNode/open'] = require('./lib/Page/AccountNode/Open.js')(chooseAccountNode, sessions, sendingFiles)
    pages['/accountNode/receiveSessionMessage'] = require('./lib/Page/AccountNode/ReceiveSessionMessage.js')(sessions)
    pages['/fileNode/requestFile'] = require('./lib/Page/FileNode/RequestFile.js')(sendingFiles)
    pages['/frontNode/addContact'] = require('./lib/Page/FrontNode/AddContact.js')(sessions)
    pages['/frontNode/changePassword'] = require('./lib/Page/FrontNode/ChangePassword.js')(sessions)
    pages['/frontNode/editProfile'] = require('./lib/Page/FrontNode/EditProfile.js')(sessions)
    pages['/frontNode/ignoreRequest'] = require('./lib/Page/FrontNode/IgnoreRequest.js')(sessions)
    pages['/frontNode/overrideContactProfile'] = require('./lib/Page/FrontNode/OverrideContactProfile.js')(sessions)
    pages['/frontNode/pullMessages'] = require('./lib/Page/FrontNode/PullMessages.js')(sessions)
    pages['/frontNode/removeContact'] = require('./lib/Page/FrontNode/RemoveContact.js')(sessions)
    pages['/frontNode/removeRequest'] = require('./lib/Page/FrontNode/RemoveRequest.js')(sessions)
    pages['/frontNode/sendFileMessage'] = require('./lib/Page/FrontNode/SendFileMessage.js')(chooseAccountNode, sessions, loadedConfig)
    pages['/frontNode/sendTextMessage'] = require('./lib/Page/FrontNode/SendTextMessage.js')(sessions)
    pages['/frontNode/signOut'] = require('./lib/Page/FrontNode/SignOut.js')(chooseAccountNode, sessions)
    pages['/frontNode/unwatchPublicProfile'] = require('./lib/Page/FrontNode/UnwatchPublicProfile.js')(chooseAccountNode, sessions)
    pages['/frontNode/watchPublicProfile'] = require('./lib/Page/FrontNode/WatchPublicProfile.js')(chooseAccountNode, sessions)
    pages['/node'] = require('./lib/Page/Node.js')(version)
    require('./lib/ScanFiles.js')('files', pages)

    require('./lib/Server.js')((req, res) => {
        Log.http(req.method + ' ' + req.url)
        var parsedUrl = url.parse(req.url, true)
        var page = pages[parsedUrl.pathname]
        if (page === undefined) page = Error404Page
        page(req, res, parsedUrl)
    })

})
