#!/bin/bash
cd `dirname $BASH_SOURCE`

if [ -f log/server.out ]
then
    echo 'INFO: Rotating log/server.out ...'
    cat log/server.out > log/server.out.1
    truncate --size 0 log/server.out
fi

if [ -f log/server.err ]
then
    echo 'INFO: Rotating log/server.err ...'
    cat log/server.err > log/server.err.1
    truncate --size 0 log/server.err
fi
