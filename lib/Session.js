var Log = require('./Log.js'),
    Message = require('./Message.js'),
    SendExpireSession = require('./SendExpireSession.js'),
    SendRemoveFile = require('./SendRemoveFile.js')

module.exports = (chooseAccountNode, prefix, token,
    username, sendingFiles, destroyListener) => {

    function callMessageListeners (response) {
        messageListeners.splice(0).forEach(listener => {
            listener(response)
        })
    }

    function destroy () {
        for (var i in files) delete sendingFiles[i]
        destroyListener()
    }

    function pushMessage (message) {

        if (messageListeners.length === 0) {
            messages.push(message)
            return
        }

        message.deliver()
        callMessageListeners([message.data])
        startTimeout()

    }

    function startTimeout () {
        timeout = setTimeout(() => {
            for (var i in files) SendRemoveFile(i, files[i])
            destroy()
            SendExpireSession(accountNode, username, token)
            messages.forEach(message => {
                message.notDeliver()
            })
        }, 1000 * 10)
    }

    function stopTimeout () {
        clearTimeout(timeout)
    }

    var accountNode = chooseAccountNode(username)

    var files = Object.create(null)

    var messages = [],
        messageListeners = []

    var timeout
    startTimeout()

    return {
        accountNode: accountNode,
        prefix: prefix,
        pushMessage: pushMessage,
        username: username,
        addFile: (token, file) => {
            files[token] = file
            sendingFiles[token] = () => {

                delete files[token]
                delete sendingFiles[token]
                var sendToken = file.sendToken
                pushMessage(Message(['requestFile', [sendToken]], () => {}, () => {}))
                SendRemoveFile(token, file)

                return {
                    sendToken: sendToken,
                    name: file.name,
                    size: file.size,
                }

            }
        },
        addMessageListener: listener => {
            messageListeners.push(listener)
            if (messageListeners.length === 1) stopTimeout()
        },
        close: () => {
            if (messageListeners.length === 0) stopTimeout()
            else callMessageListeners('INVALID_TOKEN')
            destroy()
        },
        getFiles: () => {
            return files
        },
        pullMessages: callback => {
            var responseMessages = []
            messages.splice(0).forEach(message => {
                message.deliver()
                responseMessages.push(message.data)
            })
            return responseMessages
        },
        removeMessageListener: listener => {
            var index = messageListeners.indexOf(listener)
            messageListeners.splice(index, 1)
            if (messageListeners.length === 0) startTimeout()
        },
        wake: () => {
            if (messageListeners.length !== 0) return
            stopTimeout()
            startTimeout()
        },
    }

}
