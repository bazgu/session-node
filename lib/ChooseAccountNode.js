var crypto = require('crypto')

var FetchJson = require('./FetchJson.js')

module.exports = loadedConfig => {

    var accountNodes = loadedConfig.accountNodes.map(accountNode => {

        var host = accountNode.host,
            port = accountNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'account-node-client: ' + host + ':' + port + ': ',
            fetchJson: FetchJson(host, port),
        }

    })

    return username => {
        username = username.toLowerCase()
        var digest = crypto.createHash('md5').update(username).digest()
        return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
    }

}
