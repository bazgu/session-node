const crypto = require('crypto')

module.exports = app => loadedConfig => {

    const accountNodes = loadedConfig.accountNodes.map(accountNode => {

        const host = accountNode.host,
            port = accountNode.port

        return {
            host: host,
            port: port,
            logPrefix: 'account-node: ' + host + ':' + port + ': ',
            fetchJson: app.FetchJson(host, port),
        }

    })

    return username => {
        username = username.toLowerCase()
        const digest = crypto.createHash('md5').update(username).digest()
        return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
    }

}
