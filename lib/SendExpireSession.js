var Log = require('./Log.js')

module.exports = (accountNode, username, token) => {

    var path = '/sessionNode/expireSession' +
        '?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    accountNode.fetchJson(path, response => {

        // TODO handle INVALID_USERNAME

        if (response === 'INVALID_TOKEN') return

        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

    }, () => {})

}
