module.exports = app => () => {

    app.LoadConfig(loadedConfig => {

        const url = require('url')

        const chooseAccountNode = app.ChooseAccountNode(loadedConfig)

        const sessions = Object.create(null)
        const sendingFiles = Object.create(null)

        const pages = Object.create(null)
        pages['/'] = app.Page.Index
        pages['/accountNode/checkFile'] = app.Page.AccountNode.CheckFile(sendingFiles)
        pages['/accountNode/checkSession'] = app.Page.AccountNode.CheckSession(sessions)
        pages['/accountNode/close'] = app.Page.AccountNode.Close(sessions)
        pages['/accountNode/open'] = app.Page.AccountNode.Open(chooseAccountNode, sessions, sendingFiles)
        pages['/accountNode/receiveSessionMessage'] = app.Page.AccountNode.ReceiveSessionMessage(sessions)
        pages['/fileNode/requestFile'] = app.Page.FileNode.RequestFile(sendingFiles)
        pages['/frontNode/addContact'] = app.Page.FrontNode.AddContact(sessions)
        pages['/frontNode/changePassword'] = app.Page.FrontNode.ChangePassword(sessions)
        pages['/frontNode/editProfile'] = app.Page.FrontNode.EditProfile(sessions)
        pages['/frontNode/ignoreRequest'] = app.Page.FrontNode.IgnoreRequest(sessions)
        pages['/frontNode/overrideContactProfile'] = app.Page.FrontNode.OverrideContactProfile(sessions)
        pages['/frontNode/pullMessages'] = app.Page.FrontNode.PullMessages(sessions)
        pages['/frontNode/removeContact'] = app.Page.FrontNode.RemoveContact(sessions)
        pages['/frontNode/removeRequest'] = app.Page.FrontNode.RemoveRequest(sessions)
        pages['/frontNode/sendFileMessage'] = app.Page.FrontNode.SendFileMessage(chooseAccountNode, sessions, loadedConfig)
        pages['/frontNode/sendTextMessage'] = app.Page.FrontNode.SendTextMessage(sessions)
        pages['/frontNode/signOut'] = app.Page.FrontNode.SignOut(chooseAccountNode, sessions)
        pages['/frontNode/unwatchPublicProfile'] = app.Page.FrontNode.UnwatchPublicProfile(chooseAccountNode, sessions)
        pages['/frontNode/watchPublicProfile'] = app.Page.FrontNode.WatchPublicProfile(chooseAccountNode, sessions)
        app.Static(pages)

        require('http').createServer((req, res) => {

            app.log.info('Http ' + req.method + ' ' + req.url)

            const parsed_url = url.parse(req.url, true)
            const request = {
                parsed_url, req, res,
                respond (response) {
                    app.EchoText(request, {
                        type: 'application/json',
                        content: JSON.stringify(response),
                    })
                },
            }

            const page = (() => {
                const page = pages[parsed_url.pathname]
                if (page !== undefined) return page
                return app.Error404Page
            })()

            page(request)

        }).listen(listen.port, listen.host)

    })

    app.Watch(['lib', 'static'])

    const listen = app.config.listen

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
