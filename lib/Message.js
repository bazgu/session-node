module.exports = (data, deliverListener, notDeliverListener) => {
    return {
        data: data,
        deliver: deliverListener,
        notDeliver: notDeliverListener,
    }
}
