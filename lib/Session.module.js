module.exports = app => (chooseAccountNode, prefix, token,
    username, sendingFiles, destroyListener) => {

    function callMessageListeners (response) {
        messageListeners.splice(0).forEach(listener => {
            listener(response)
        })
    }

    function destroy () {
        for (const i in files) delete sendingFiles[i]
        destroyListener()
    }

    function pushMessage (message) {

        if (messageListeners.length === 0) {
            messages.push(message)
            return
        }

        message.deliver()
        callMessageListeners([message.data])
        startTimeout()

    }

    function startTimeout () {
        timeout = setTimeout(() => {
            for (const i in files) app.SendRemoveFile(i, files[i])
            destroy()
            app.SendExpireSession(accountNode, username, token)
            messages.forEach(message => {
                message.notDeliver()
            })
        }, 1000 * 10)
    }

    function stopTimeout () {
        clearTimeout(timeout)
    }

    const accountNode = chooseAccountNode(username)

    const files = Object.create(null)

    const messages = [],
        messageListeners = []

    let timeout
    startTimeout()

    return {
        accountNode: accountNode,
        prefix: prefix,
        pushMessage: pushMessage,
        username: username,
        addFile: (token, file) => {
            files[token] = file
            sendingFiles[token] = () => {

                delete files[token]
                delete sendingFiles[token]
                const sendToken = file.sendToken
                pushMessage(app.Message(['requestFile', [sendToken]], () => {}, () => {}))
                app.SendRemoveFile(token, file)

                return {
                    sendToken: sendToken,
                    name: file.name,
                    size: file.size,
                }

            }
        },
        addMessageListener: listener => {
            messageListeners.push(listener)
            if (messageListeners.length === 1) stopTimeout()
        },
        close: () => {
            if (messageListeners.length === 0) stopTimeout()
            else callMessageListeners('INVALID_TOKEN')
            destroy()
        },
        getFiles: () => {
            return files
        },
        pullMessages () {
            const responseMessages = []
            messages.splice(0).forEach(message => {
                message.deliver()
                responseMessages.push(message.data)
            })
            return responseMessages
        },
        removeMessageListener: listener => {
            const index = messageListeners.indexOf(listener)
            if (index === -1) throw new Error
            messageListeners.splice(index, 1)
            if (messageListeners.length === 0) startTimeout()
        },
        wake: () => {
            if (messageListeners.length !== 0) return
            stopTimeout()
            startTimeout()
        },
    }

}
