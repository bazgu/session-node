module.exports = app => (accountNode, username, token) => {

    const logPrefix = accountNode.logPrefix + 'sessionNode/expireSession: '

    const path = '/sessionNode/expireSession' +
        '?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    accountNode.fetchJson(path, response => {

        // TODO handle INVALID_USERNAME

        if (response === 'INVALID_TOKEN') return

        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

    }, () => {})

}
