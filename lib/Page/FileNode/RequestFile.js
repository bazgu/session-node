module.exports = sendingFiles => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var token = parsedUrl.query.token
        var requestFile = sendingFiles[token]
        if (requestFile === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        res.end(JSON.stringify(requestFile()))

    }
}
