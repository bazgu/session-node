module.exports = sendingFiles => request => {

    const token = request.parsed_url.query.token
    const requestFile = sendingFiles[token]
    if (requestFile === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    request.respond(requestFile())

}
