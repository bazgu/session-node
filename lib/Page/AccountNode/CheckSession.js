module.exports = sessions => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var session = sessions[parsedUrl.query.token]
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        res.end('true')

    }
}
