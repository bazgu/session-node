module.exports = sessions => request => {

    const session = sessions[request.parsed_url.query.token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    request.respond(true)

}
