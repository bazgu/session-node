module.exports = sendingFiles => request => {

    const requestFile = sendingFiles[request.parsed_url.query.token]
    if (requestFile === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    request.respond(true)

}
