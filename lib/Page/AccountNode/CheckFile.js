module.exports = sendingFiles => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var requestFile = sendingFiles[parsedUrl.query.token]
        if (requestFile === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        res.end('true')

    }
}
