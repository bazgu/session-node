var Message = require('../../Message.js'),
    ReadText = require('../../ReadText.js')

module.exports = sessions => {
    return (req, res, parsedUrl) => {
        ReadText(req, requestText => {

            res.setHeader('Content-Type', 'application/json')

            try {
                var requestObject = JSON.parse(requestText)
            } catch (e) {
                res.end('"BAD_REQUEST"')
                return
            }

            var tokens = parsedUrl.query.tokens
            if (tokens === undefined) {
                res.end('[]')
                return
            }

            ;(() => {

                function check () {
                    if (numPending !== 0) return
                    res.end(JSON.stringify(invalidTokens))
                }

                var numPending = 0
                var invalidTokens = []
                tokens.split(',').forEach(token => {

                    var session = sessions[token]
                    if (session === undefined) {
                        invalidTokens.push(token)
                        return
                    }

                    session.pushMessage(Message(requestObject, () => {
                        numPending--
                        check()
                    }, () => {
                        invalidTokens.push(token)
                        numPending--
                        check()
                    }))
                    numPending++

                })
                check()

            })()

        })
    }
}
