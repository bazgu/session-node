module.exports = app => sessions => request => {
    app.ReadText(request.req, requestText => {

        let requestObject
        try {
            requestObject = JSON.parse(requestText)
        } catch (e) {
            request.respond('BAD_REQUEST')
            return
        }

        const tokens = request.parsed_url.query.tokens
        if (tokens === undefined) {
            request.respond([])
            return
        }

        ;(() => {

            function check () {
                if (numPending !== 0) return
                request.respond(invalidTokens)
            }

            let numPending = 0
            const invalidTokens = []
            tokens.split(',').forEach(token => {

                const session = sessions[token]
                if (session === undefined) {
                    invalidTokens.push(token)
                    return
                }

                session.pushMessage(app.Message(requestObject, () => {
                    numPending--
                    check()
                }, () => {
                    invalidTokens.push(token)
                    numPending--
                    check()
                }))
                numPending++

            })
            check()

        })()

    })
}
