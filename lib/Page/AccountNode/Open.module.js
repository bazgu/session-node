const crypto = require('crypto')

module.exports = app => (chooseAccountNode, sessions, sendingFiles) => request => {

    const query = request.parsed_url.query

    const prefix = query.prefix

    const token = prefix + '_' + crypto.randomBytes(20).toString('hex')
    const session = app.Session(chooseAccountNode, prefix, token, query.username, sendingFiles, () => {
        delete sessions[token]
        app.log.info('Session ' + token + ' closed')
    })

    sessions[token] = session
    app.log.info('Session ' + token + ' opened')

    request.respond(token)

}
