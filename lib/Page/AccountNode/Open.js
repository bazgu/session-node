var crypto = require('crypto')

var Log = require('../../Log.js'),
    Session = require('../../Session.js')

module.exports = (chooseAccountNode, sessions, sendingFiles) => {
    return (req, res, parsedUrl) => {

        var query = parsedUrl.query

        var prefix = query.prefix

        var token = prefix + '_' + crypto.randomBytes(20).toString('hex')
        var session = Session(chooseAccountNode, prefix, token, query.username, sendingFiles, () => {
            delete sessions[token]
            Log.info('Session ' + token + ' closed')
        })

        sessions[token] = session
        Log.info('Session ' + token + ' opened')

        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify(token))

    }
}
