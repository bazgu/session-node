module.exports = sessions => request => {

    const query = request.parsed_url.query

    const tokens = query.tokens
    if (tokens !== undefined) {
        tokens.split(',').forEach(token => {
            const session = sessions[token]
            if (session !== undefined) session.close()
        })
    }

    request.respond(true)

}
