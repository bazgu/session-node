var ProxyAccount = require('./lib/ProxyAccount.js')

module.exports = (chooseAccountNode, sessions) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query
        var token = query.token

        var username = query.username
        if (username === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var session = sessions[token]
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        session.wake()

        var queryString = '?username=' + encodeURIComponent(username) +
            '&token=' + encodeURIComponent(token)

        ProxyAccount(chooseAccountNode(username),
            req, res, 'watchPublicProfile', queryString)

    }
}
