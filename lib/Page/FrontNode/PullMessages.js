module.exports = sessions => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var session = sessions[parsedUrl.query.token]
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var messages = session.pullMessages()
        if (messages.length > 0) {
            session.wake()
            res.end(JSON.stringify(messages))
            return
        }

        ;(() => {

            function listener (messages) {
                clearTimeout(timeout)
                res.end(JSON.stringify(messages))
            }

            session.addMessageListener(listener)

            var timeout = setTimeout(() => {
                session.removeMessageListener(listener)
                res.end('"NOTHING_TO_PULL"')
            }, 1000 * 30)

            req.on('close', () => {
                clearTimeout(timeout)
                session.removeMessageListener(listener)
            })

        })()

    }
}
