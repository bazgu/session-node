module.exports = sessions => request => {

    const req = request.req

    const session = sessions[request.parsed_url.query.token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const messages = session.pullMessages()
    if (messages.length > 0) {
        session.wake()
        request.respond(messages)
        return
    }

    ;(() => {

        function listener (messages) {
            clearTimeout(timeout)
            req.off('close', req_close)
            request.respond(messages)
        }

        function req_close () {
            clearTimeout(timeout)
            session.removeMessageListener(listener)
        }

        session.addMessageListener(listener)

        const timeout = setTimeout(() => {
            session.removeMessageListener(listener)
            req.off('close', req_close)
            request.respond('NOTHING_TO_PULL')
        }, 1000 * 30)

        req.on('close', req_close)

    })()

}
