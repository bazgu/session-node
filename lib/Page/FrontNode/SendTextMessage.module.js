module.exports = app => sessions => request => {

    const query = request.parsed_url.query
    const token = query.token

    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    session.wake()

    const queryString =
        '?username=' + encodeURIComponent(session.username) +
        '&contactUsername=' + encodeURIComponent(query.username) +
        '&text=' + encodeURIComponent(query.text) +
        '&token=' + encodeURIComponent(token)

    app.ProxyAccount(session.accountNode,
        request, 'sendTextMessage', queryString)

}
