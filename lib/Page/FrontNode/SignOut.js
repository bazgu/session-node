var Log = require('../../Log.js'),
    SendRemoveFile = require('../../SendRemoveFile.js')

module.exports = (chooseAccountNode, sessions) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var token = query.token
        var session = sessions[token]
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var files = session.getFiles()
        for (var i in files) SendRemoveFile(i, files[i])

        session.close()
        res.end('true')

        ;(() => {

            var path = '/sessionNode/signOut' +
                '?username=' + encodeURIComponent(session.username) +
                '&token=' + encodeURIComponent(token)

            var abort = session.accountNode.fetchJson(path, response => {

                req.removeListener('close', abort)

                // TODO handle INVALID_USERNAME

                if (response === true) return
                Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

            }, () => {})

            req.on('close', abort)

        })()

    }
}
