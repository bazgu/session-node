module.exports = app => (chooseAccountNode, sessions) => request => {

    const query = request.parsed_url.query

    const token = query.token
    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const files = session.getFiles()
    for (const i in files) app.SendRemoveFile(i, files[i])

    session.close()
    request.respond(true)

    ;(() => {

        const logPrefix = session.accountNode.logPrefix + 'sessionNode/signOut: '

        const path = '/sessionNode/signOut' +
            '?username=' + encodeURIComponent(session.username) +
            '&token=' + encodeURIComponent(token)

        session.accountNode.fetchJson(path, response => {

            // TODO handle INVALID_USERNAME

            if (response === true) return
            app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

        }, () => {})

    })()

}
