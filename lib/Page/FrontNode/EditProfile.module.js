module.exports = app => sessions => request => {

    const query = request.parsed_url.query
    const token = query.token

    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    session.wake()

    const queryString =
        '?username=' + encodeURIComponent(session.username) +
        '&fullName=' + encodeURIComponent(query.fullName) +
        '&fullNamePrivacy=' + encodeURIComponent(query.fullNamePrivacy) +
        '&email=' + encodeURIComponent(query.email) +
        '&emailPrivacy=' + encodeURIComponent(query.emailPrivacy) +
        '&phone=' + encodeURIComponent(query.phone) +
        '&phonePrivacy=' + encodeURIComponent(query.phonePrivacy) +
        '&timezone=' + encodeURIComponent(query.timezone) +
        '&timezonePrivacy=' + encodeURIComponent(query.timezonePrivacy) +
        '&token=' + encodeURIComponent(token)

    app.ProxyAccount(session.accountNode, request, 'editProfile', queryString)

}
