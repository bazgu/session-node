const crypto = require('crypto')

module.exports = app => (chooseAccountNode, sessions, loadedConfig) => {
    const fileNodes = loadedConfig.fileNodes
    return request => {

        const req = request.req

        const query = request.parsed_url.query
        const token = query.token

        const session = sessions[token]
        if (session === undefined) {
            request.respond('INVALID_TOKEN')
            return
        }

        const name = query.name
        if (name === undefined) {
            request.respond('INVALID_NAME')
            return
        }

        const size = parseInt(query.size, 10)
        if (!isFinite(size)) {
            request.respond('INVALID_SIZE')
            return
        }

        const contactUsername = query.username
        const index = Math.floor(Math.random() * fileNodes.length)
        const receiveToken = index + '_' + session.prefix + '_' + crypto.randomBytes(20).toString('hex')
        const sendToken = index + '_' + crypto.randomBytes(20).toString('hex')

        session.wake()
        session.addFile(receiveToken, {
            accountNode: chooseAccountNode(contactUsername),
            username: contactUsername,
            sendToken: sendToken,
            name: name,
            size: size,
        })

        ;(() => {

            const path = '/sessionNode/sendFileMessage' +
                '?username=' + encodeURIComponent(session.username) +
                '&contactUsername=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size) +
                '&token=' + encodeURIComponent(token) +
                '&fileToken=' + encodeURIComponent(receiveToken)

            const abort = session.accountNode.fetchJson(path, response => {

                req.removeListener('close', abort)

                // TODO handle INVALID_USERNAME, INVALID_NAME, INVALID_SIZE

                if (response === 'INVALID_TOKEN' || response === 'INVALID_CONTACT_USERNAME') {
                    request.respond(response)
                    return
                }

                request.respond({
                    time: response.time,
                    token: response.token,
                    sendToken: sendToken,
                })

            }, () => {
                app.Error500Page(request)
            })

            req.on('close', abort)

        })()

    }
}
