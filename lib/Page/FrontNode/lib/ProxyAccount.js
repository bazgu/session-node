var http = require('http')

var Error500Page = require('../../../Error500Page.js'),
    Log = require('../../../Log.js')

module.exports = (accountNode, req, res, method, queryString) => {

    function closeListener () {
        proxyReq.removeListener('error', errorListener)
        proxyReq.on('error', () => {})
        proxyReq.abort()
    }

    function errorListener (err) {
        Log.error(logPrefix + err.code)
        Error500Page(res)
    }

    method = 'sessionNode/' + method

    var logPrefix = accountNode.logPrefix + method + ': '

    var proxyReq = http.get({
        host: accountNode.host,
        port: accountNode.port,
        path: '/' + method + queryString,
    }, proxyRes => {

        proxyReq.removeListener('error', errorListener)
        proxyReq.on('error', () => {})

        req.removeListener('close', closeListener)
        req.on('close', () => {
            proxyReq.abort()
        })

        var statusCode = proxyRes.statusCode
        if (statusCode !== 200) {
            Log.error(logPrefix + 'HTTP status code ' + statusCode)
            Error500Page(res)
            return
        }

        proxyRes.pipe(res)
        proxyRes.on('error', err => {
            Log.error(logPrefix + 'Transfer error ' + err.code)
        })

    })
    proxyReq.on('error', errorListener)

    req.on('close', closeListener)

}


