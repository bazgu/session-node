module.exports = app => (chooseAccountNode, sessions) => request => {

    const query = request.parsed_url.query
    const token = query.token

    const username = query.username
    if (username === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    session.wake()

    const queryString = '?username=' + encodeURIComponent(username) +
        '&token=' + encodeURIComponent(token)

    app.ProxyAccount(chooseAccountNode(username),
        request, 'unwatchPublicProfile', queryString)

}
