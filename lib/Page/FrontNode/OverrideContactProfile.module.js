module.exports = app => sessions => request => {

    const query = request.parsed_url.query
    const token = query.token

    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    session.wake()

    let queryString =
        '?username=' + encodeURIComponent(session.username) +
        '&contactUsername=' + encodeURIComponent(query.username) +
        '&fullName=' + encodeURIComponent(query.fullName) +
        '&email=' + encodeURIComponent(query.email) +
        '&phone=' + encodeURIComponent(query.phone)

    const timezone = query.timezone
    if (timezone !== undefined) {
        queryString += '&timezone=' + encodeURIComponent(timezone)
    }

    queryString += '&token=' + encodeURIComponent(token)

    app.ProxyAccount(session.accountNode,
        request, 'overrideContactProfile', queryString)

}
