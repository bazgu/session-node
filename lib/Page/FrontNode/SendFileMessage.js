var crypto = require('crypto')

var Error500Page = require('../../Error500Page.js')

module.exports = (chooseAccountNode, sessions, loadedConfig) => {
    var fileNodes = loadedConfig.fileNodes
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query
        var token = query.token

        var session = sessions[token]
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var name = query.name
        if (name === undefined) {
            res.end('"INVALID_NAME"')
            return
        }

        var size = parseInt(query.size, 10)
        if (!isFinite(size)) {
            res.end('"INVALID_SIZE"')
            return
        }

        var contactUsername = query.username
        var index = Math.floor(Math.random() * fileNodes.length)
        var receiveToken = index + '_' + session.prefix + '_' + crypto.randomBytes(20).toString('hex')
        var sendToken = index + '_' + crypto.randomBytes(20).toString('hex')

        session.wake()
        session.addFile(receiveToken, {
            accountNode: chooseAccountNode(contactUsername),
            username: contactUsername,
            sendToken: sendToken,
            name: name,
            size: size,
        })

        ;(() => {

            var path = '/sessionNode/sendFileMessage' +
                '?username=' + encodeURIComponent(session.username) +
                '&contactUsername=' + encodeURIComponent(contactUsername) +
                '&name=' + encodeURIComponent(name) +
                '&size=' + encodeURIComponent(size) +
                '&token=' + encodeURIComponent(token) +
                '&fileToken=' + encodeURIComponent(receiveToken)

            var abort = session.accountNode.fetchJson(path, response => {

                req.removeListener('close', abort)

                // TODO handle INVALID_USERNAME, INVALID_NAME, INVALID_SIZE

                if (response === 'INVALID_TOKEN' || response === 'INVALID_CONTACT_USERNAME') {
                    res.end(responseText)
                    return
                }

                res.end(JSON.stringify({
                    time: response.time,
                    token: response.token,
                    sendToken: sendToken,
                }))

            }, () => {
                Error500Page(res)
            })

            req.on('close', abort)

        })()

    }
}
