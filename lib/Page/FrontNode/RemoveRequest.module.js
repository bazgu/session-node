module.exports = app => sessions => request => {

    const query = request.parsed_url.query
    const token = query.token

    const session = sessions[token]
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    session.wake()

    const queryString =
        '?username=' + encodeURIComponent(session.username) +
        '&requestUsername=' + encodeURIComponent(query.username) +
        '&token=' + encodeURIComponent(token)

    app.ProxyAccount(session.accountNode,
        request, 'removeRequest', queryString)

}
