var methods = [{
    path: 'accountNode/checkFile',
    queryString: {
        token: { type: 'string' },
    },
}, {
    path: 'accountNode/checkSession',
    queryString: {
        token: { type: 'string' },
    },
}, {
    path: 'accountNode/close',
    queryString: {
        tokens: { type: 'string' },
    },
}, {
    path: 'accountNode/open',
    queryString: {
        username: { type: 'string' },
        prefix: { type: 'string' },
    },
}, {
    path: 'accountNode/receiveSessionMessage',
    queryString: {
        tokens: { type: 'string' },
    },
}, {
    path: 'fileNode/requestFile',
    queryString: {
        token: { type: 'string' },
    },
}, {
    path: 'frontNode/addContact',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
        fullName: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        timezone: { type: 'number' },
    },
}, {
    path: 'frontNode/changePassword',
    queryString: {
        token: { type: 'string' },
        currentPassword: { type: 'string' },
        newPassword: { type: 'string' },
    },
}, {
    path: 'frontNode/editProfile',
    queryString: {
        token: { type: 'string' },
        fullName: { type: 'string' },
        fullNamePrivacy: { type: 'string' },
        email: { type: 'string' },
        emailPrivacy: { type: 'string' },
        phone: { type: 'string' },
        phonePrivacy: { type: 'string' },
        timezone: { type: 'number' },
    },
}, {
    path: 'frontNode/ignoreRequest',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
    },
}, {
    path: 'frontNode/overrideContactProfile',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
        fullName: { type: 'string' },
        email: { type: 'string' },
        phone: { type: 'string' },
        timezone: { type: 'string' },
    },
}, {
    path: 'frontNode/pullMessages',
    queryString: {
        token: { type: 'string' },
    },
}, {
    path: 'frontNode/removeContact',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
    },
}, {
    path: 'frontNode/removeRequest',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
    },
}, {
    path: 'frontNode/sendFileMessage',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
        name: { type: 'string' },
        size: { type: 'number' },
    },
}, {
    path: 'frontNode/sendTextMessage',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
        text: { type: 'string' },
    },
}, {
    path: 'frontNode/signOut',
    queryString: {
        token: { type: 'string' },
    },
}, {
    path: 'frontNode/unwatchPublicProfile',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
    },
}, {
    path: 'frontNode/watchPublicProfile',
    queryString: {
        token: { type: 'string' },
        username: { type: 'string' },
    },
}]

var content =
    '<!DOCTYPE html>' +
    '<html>' +
        '<head>' +
            '<title>Session Node Documentation</title>' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="stylesheet" type="text/css" href="index.css" />' +
        '</head>' +
        '<body>' +
            '<h1 class="title">Session Node Documentation</h1>' +
            '<div class="index">' +
                '<h2>Method Reference</h2>' +
                '<ul>' +
                    methods.map(method => {
                        return '<li class="method">' +
                            '<a class="link" href="#' + method.path + '">' +
                                method.path +
                            '</a>' +
                        '</li>'
                    }).join('') +
                '</ul>' +
            '</div>' +
            '<div class="content">' +
                methods.map(method => {
                    var queryString = method.queryString
                    return '<h2 id="' + method.path +'">' +
                            method.path +
                        '</h2>' +
                        '<div style="padding-left: 20px">' +
                            '<div>' +
                                '<h3>Query String</h3>' +
                                '<ul>' +
                                    Object.keys(queryString).map(name => {
                                        var item = queryString[name]
                                        return '<li>' +
                                                '&bull; <code>' + name + ' - (' + item.type + ')</code>' +
                                            '</li>'
                                    }).join('') +
                                '</ul>' +
                            '</div>' +
                        '</div>'
                }).join('<br />') +
            '</div>' +
            '<div style="clear: both"></div>' +
        '</body>' +
    '</html>'

module.exports = (req, res) => {
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(content)
}
