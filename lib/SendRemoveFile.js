var Log = require('./Log.js')

module.exports = (token, file) => {

    var path = '/sessionNode/removeFile' +
        '?username=' + encodeURIComponent(file.username) +
        '&token=' + encodeURIComponent(token)

    file.accountNode.fetchJson(path, response => {

        // TODO handle INVALID_USERNAME

        if (response === 'INVALID_TOKEN') return

        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

    }, () => {})

}
