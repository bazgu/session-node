module.exports = app => (token, file) => {

    const logPrefix = file.accountNode + 'sessionNode/removeFile: '

    const path = '/sessionNode/removeFile' +
        '?username=' + encodeURIComponent(file.username) +
        '&token=' + encodeURIComponent(token)

    file.accountNode.fetchJson(path, response => {

        // TODO handle INVALID_USERNAME

        if (response === 'INVALID_TOKEN') return

        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))

    }, () => {})

}
