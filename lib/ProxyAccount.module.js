const http = require('http')

module.exports = app => (accountNode, request, method, queryString) => {

    function closeListener () {
        proxyReq.removeListener('error', errorListener)
        proxyReq.on('error', () => {})
        proxyReq.abort()
    }

    function errorListener (err) {
        app.log.error(logPrefix + err.code)
        app.Error500Page(request)
    }

    const { req, res } = request

    method = 'sessionNode/' + method

    const logPrefix = accountNode.logPrefix + method + ': '

    const proxyReq = http.get({
        host: accountNode.host,
        port: accountNode.port,
        path: '/' + method + queryString,
    }, proxyRes => {

        proxyReq.removeListener('error', errorListener)
        proxyReq.on('error', () => {})

        req.removeListener('close', closeListener)
        req.on('close', () => {
            proxyReq.abort()
        })

        const statusCode = proxyRes.statusCode
        if (statusCode !== 200) {
            app.log.error(logPrefix + 'HTTP status code ' + statusCode)
            app.Error500Page(request)
            return
        }

        proxyRes.pipe(res)
        proxyRes.on('error', err => {
            app.log.error(logPrefix + 'Transfer error ' + err.code)
        })

    })
    proxyReq.on('error', errorListener)

    req.on('close', closeListener)

}


